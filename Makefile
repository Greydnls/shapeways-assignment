build-container:
    composer install
	docker build .
	docker-compose up -d
	docker exec -i commentary_db_1 mysql -u commentary -psecurelol -e "create database IF NOT EXISTS commentary"
	docker exec -i commentary_db_1 mysql -u commentary -psecurelol commentary < db.sql
	docker exec -it commentary_app_1 php /var/www/html/fill_db.php
