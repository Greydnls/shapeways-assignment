<?php

namespace Grey\Commentary\Entity;

use Doctrine\ORM\Mapping as ORM;
use Grey\Commentary\Entity\Behavior\HasCreatedDate;

/**
 * @ORM\Entity()
 * @ORM\Table(name="comments")
 *
 * @ORM\HasLifecycleCallbacks
 */
class Comment
{
    use HasCreatedDate;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(
     *     type="string"
     * )
     *
     * @var string
     */
    private $content;

    /**
     * @ORM\Column(
     *     type="boolean",
     *     name="has_been_read"
     * )
     *
     * @var bool
     */
    private $hasBeenRead = false;

    /**
     * @ORM\ManyToOne(
     *      targetEntity="Grey\Commentary\Entity\User",
     *      inversedBy="comments"
     * )
     * @ORM\JoinColumn(name="author_id", referencedColumnName="id", nullable=false)
     *
     * @var User
     */
    private $author;

    /**
     * @ORM\ManyToOne(targetEntity="Grey\Commentary\Entity\Product", inversedBy="comments")
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id", nullable=false)
     *
     * @var User
     */
    private $product;

    public function __construct(User $author, Product $product, $content)
    {
        $this->author = $author;
        $this->content = $content;
        $this->product = $product;
    }

    public function markRead()
    {
        $this->hasBeenRead = true;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @return boolean
     */
    public function hasBeenRead()
    {
        return $this->hasBeenRead;
    }

    /**
     * @return User
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @return User
     */
    public function getProduct()
    {
        return $this->product;
    }
}