<?php

namespace Grey\Commentary\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Grey\Commentary\Entity\Behavior\HasCreatedDate;

/**
 * @ORM\Entity()
 * @ORM\Table(name="products")
 */
class Product
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(
     *     type="string"
     * )
     *
     * @var string
     */
    private $name;

    /**
     * @ORM\Column(
     *     type="string"
     * )
     *
     * @var string
     */
    private $description;

    /**
     * @ORM\Column(
     *     type="string",
     *     name="image_url"
     * )
     *
     * @var string
     */
    private $imageUrl;

    /**
     * @ORM\OneToMany(targetEntity="Grey\Commentary\Entity\Comment", mappedBy="product")
     *
     * @var Comment[]|ArrayCollection
     */
    private $comments;

    public function __construct(string $name, string $description, string $imageUrl)
    {
        $this->comments = new ArrayCollection();
        $this->name = $name;
        $this->description = $description;
        $this->imageUrl = $imageUrl;
    }

    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName() : string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getDescription() : string
    {
        return $this->description;
    }

    /**
     * @return Collection|Comment[]
     */
    public function getComments() : Collection
    {
        return $this->comments;
    }

    public function getUnreadComments() : Collection
    {
        return $this->comments->filter(function(Comment $comment){
            return $comment->hasBeenRead() === false;
        });
    }

    /**
     * @param Comment $comment
     */
    public function addComment(Comment $comment)
    {
        $this->comments->add($comment);
    }
}