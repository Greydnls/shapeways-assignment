<?php

namespace Grey\Commentary\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="users")
 */
class User
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(
     *     type="string"
     * )
     *
     * @var string
     */
    private $email;

    /**
     * @ORM\Column(
     *     type="string"
     * )
     *
     * @var string
     */
    private $name;

    public function __construct($name, $email)
    {

        $this->name = $name;
        $this->email = $email;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}