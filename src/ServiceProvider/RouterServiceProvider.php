<?php

namespace Grey\Commentary\ServiceProvider;

use Assert\Assertion;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Grey\Commentary\Entity;
use League\Container\ServiceProvider\AbstractServiceProvider;
use League\Plates\Engine;
use League\Route\RouteCollection;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Zend\Diactoros\Response;

class RouterServiceProvider extends AbstractServiceProvider
{
    protected $provides = [
        RouteCollection::class
    ];

    public function register()
    {
        $this->container->share(RouteCollection::class, function () : RouteCollection {
            $router = new \League\Route\RouteCollection($this->container);

            $router->get('/', function (ServerRequestInterface $request, ResponseInterface $response) : ResponseInterface {
                $response = new Response;

                $em = $this->container->get(EntityManager::class);

                /** @var EntityRepository $r */
                $r = $em->getRepository(Entity\Product::class);
                $results = $r->findAll();

                $templates = new Engine(BASE_DIR."/views");

                $response->getBody()->write(
                    $templates->render('product-list', ['products' => $results])
                );

                return $response;
            });

            $router->get('/products/{id}', function (ServerRequestInterface $request, ResponseInterface $response, array $vars) : ResponseInterface {
                $response = new Response;

                $em = $this->container->get(EntityManager::class);

                /** @var EntityRepository $r */
                $r = $em->getRepository(Entity\Product::class);
                $result = $r->find($vars['id']);

                $templates = new Engine(BASE_DIR."/views");

                $response->getBody()->write(
                    $templates->render('single-product', ['product' => $result])
                );

                return $response;
            });


            $router->post('products/{id}/comments',
                function (ServerRequestInterface $request, ResponseInterface $response, array $vars)  : ResponseInterface {

                    $postBody = $request->getParsedBody();

                    try{
                        Assertion::count(array_diff(['name', 'email', 'comment'], array_keys($postBody)), 0);
                        \Assert\that($postBody)->all()->notEmpty();
                    } catch (\Exception $e){
                        return new Response\JsonResponse(["error" => $e->getMessage()], 400);
                    }

                    $em = $this->container->get(EntityManager::class);

                    /** @var EntityRepository $productRepository */
                    $productRepository = $em->getRepository(Entity\Product::class);

                    /** @var Entity\Product $product */
                    $product = $productRepository->find($vars['id']);

                    /** @var EntityRepository $userRepository */
                    $userRepository = $em->getRepository(Entity\User::class);
                    $user = $userRepository->findBy(['email' => $postBody['email']])
                        ?: new Entity\User($postBody['name'], $postBody['email']);

                    $comment = new Entity\Comment($user, $product, $postBody['comment']);

                    $em->persist($user);
                    $em->persist($comment);
                    $em->flush();

                    $result = [
                        'id' => $comment->getId(),
                        'content' => $comment->getContent(),
                        'has_been_read' => $comment->hasBeenRead(),
                        'author' => [
                            'name' => $user->getName(),
                            'email' => $user->getEmail()
                        ]
                    ];

                    return new Response\JsonResponse($result);
                });

            return $router;
        });
    }
}