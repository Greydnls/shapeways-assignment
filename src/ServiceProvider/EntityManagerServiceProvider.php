<?php


namespace Grey\Commentary\ServiceProvider;

use Doctrine\DBAL\Tools\Console\Helper\ConnectionHelper;
use Doctrine\ORM\Tools\Console\Helper\EntityManagerHelper;
use League\Container\ServiceProvider\AbstractServiceProvider;
use Doctrine\ORM;
use Doctrine\Common;
use Symfony\Component\Console\Helper\HelperSet;

class EntityManagerServiceProvider extends AbstractServiceProvider
{
    protected $provides = [
        ORM\EntityManager::class,
        HelperSet::class
    ];

    /**
     * Use the register method to register items with the container via the
     * protected $this->container property or the `getContainer` method
     * from the ContainerAwareTrait.
     */
    public function register()
    {
        $this->container->share(ORM\EntityManager::class, function () {
            $entityDirectory = __DIR__ . '/../Entity';
            $proxyDirectory = __DIR__ . '/../proxies/';

            $cache = new Common\Cache\ArrayCache();
            $config = new ORM\Configuration();
            $annotationDriver = $config->newDefaultAnnotationDriver($entityDirectory, false);
            $config->setMetadataDriverImpl($annotationDriver);
            $config->setQueryCacheImpl($cache);
            $config->setMetadataCacheImpl($cache);
            $config->setProxyDir($proxyDirectory);
            $config->setProxyNamespace('Grey\Commentary\Proxies');

            $dbParams = [
                'driver' => 'pdo_mysql',
                'user' => getenv('MYSQL_USER'),
                'password' => getenv('MYSQL_PASSWORD'),
                'host' => 'db',
                'dbname' => 'commentary',             ];
            return  ORM\EntityManager::create($dbParams, $config);
        });
        $this->container->share(HelperSet::class, function () {
            $entityManager = $this->container->get(ORM\EntityManager::class);
            return new HelperSet([
                'db' => new ConnectionHelper($entityManager->getConnection()),
                'em' => new EntityManagerHelper($entityManager),
            ]);
        });
    }
}