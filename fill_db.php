<?php

use Faker\Factory as Faker;
use Grey\Commentary\Entity\Product;

require __DIR__ . "/vendor/autoload.php";

$container = new \League\Container\Container();
$container->addServiceProvider(new \Grey\Commentary\ServiceProvider\EntityManagerServiceProvider());

$entityManager = $container->get(\Doctrine\ORM\EntityManager::class);

for ($i = 0; $i <= 5; $i++){
    $faker = Faker::create();

    $product = new Product($faker->word, $faker->sentence, $faker->imageUrl());
    $entityManager->persist($product);
}

$entityManager->flush();