# Shapeways Assignment:

To runn application:
Have docker install
Have composer install
run:
`make build-container`

Access application at `localhost:1234`

## 1. Design a MYSQL database table or tables to hold user comments on a product. Explain how it works, and how it’s used.
See db.sql

```
comments (
    id INT AUTO_INCREMENT NOT NULL,
    author_id INT NOT NULL,
    product_id INT NOT NULL,
    content VARCHAR(255) NOT NULL,
    has_been_read TINYINT(1) NOT NULL,
    created DATETIME NOT NULL
    )

products (
    id INT AUTO_INCREMENT NOT NULL,
    name VARCHAR(255) NOT NULL,
    description VARCHAR(255) NOT NULL,
    image_url VARCHAR(255) NOT NULL
    )

users (
    id INT AUTO_INCREMENT NOT NULL,
    email VARCHAR(255) NOT NULL,
    name VARCHAR(255) NOT NULL
    )
```

The comments table holds foreign keys to both Products and User tables.

## 2. How would you get the number of unread comments per product for a user?
By storing "has_been_read" as a boolean value on the comments table you can easily query for unread comments.

## 3. Write a request handler that adds a comment to a product.
See lines 63-98 in RouterServiceProvider

## 4. Write a javascript function that submits a comment form
See Lines 52-68 in /views/single-product.php

## 5. Implement a server-side code that renders a page with the form in it. Explain your choices.
See lines 44-60 in RouterServiceProvider

## 6. How would you test the above?
Unit tests could be written for each of the entities to ensure they're being instantiated correctly.

Integration testing could be written to ensure that when a Comment is saved
1: The correct user is associated (if exists) or created (if not exists)
2: The correct product is associated
3: The correct values are saved.

Functional testing could be used with something like Selenium to ensure that the form is working as
expected and after comments are submitted they're correctly saved.