<?php $this->layout('layouts/main') ?>
<a href="/" class="btn btn-default"> < Back</a>
<hr>
<h2>Product Detail</h2>
<div class="card">
    <div class="card-block">
        <h4 class="card-title"><?=$product->getName()?></h4>
        <p class="card-text"><?=$product->getDescription()?></p>
    </div>
</div>
<div class="card">
    <div class="card-block">
        <h5 class="card-title">Comments</h5>

        <ul class="list-group list-group-flush" id="comment-list-group">
            <? if ($product->getComments()->count() > 0): ?>
                <? foreach ($product->getComments() as $comment): ?>
                    <li class="list-group-item">
                        <?= $comment->getContent(); ?>
                        <small>by <i><?=$comment->getAuthor()->getName(); ?></i></small>
                        <? if (!$comment->hasBeenRead()) : ?>
                            <span class="tag tag-primary">New!</span>
                        <? endif; ?>
                    </li>
                <? endforeach; ?>
            <? else: ?>
                <li class="list-group-item">No Comments</li>
            <? endif; ?>

        </ul>
    </div>
</div>

<form id="comment-form">
    <div class="form-group">
        <label for="comment">Comment</label>
        <textarea class="form-control" name="comment" id="comment" rows="3"></textarea>
    </div>
    <div class="form-group">
        <label for="name">Name</label>
        <input type="text" class="form-control" name="name" id="name" aria-describedby="nameHelp" placeholder="Enter Name">
    </div>
    <div class="form-group">
        <label for="email">Email address</label>
        <input type="email" class="form-control" name="email" id="email" aria-describedby="emailHelp" placeholder="Enter email">
    </div>
    <div id="submit-btn" class="btn btn-primary">Submit</div>
</form>

<script>

    $("#submit-btn").click(function(){
        $.ajax({
            type: "POST",
            data: $("#comment-form").serialize(),
            url: "/products/<?=$product->getId()?>/comments",
            success: function(result){
                $('#comment-form').find("input[type=text], textarea, input[type=email]").val("");
                $('#comment-list-group').append('<li class="list-group-item">' +
                    result.content +
                    '<small> by <i>' +
                    result.author.name +
                    '</i></small>' +
                    ' <span class="tag tag-primary">New!</span></li>')
            }
        });
    });
</script>