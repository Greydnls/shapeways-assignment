<?php $this->insert('includes/header') ?>

<div class="container">
    <div class="row">
        <div class="col-sm-8">
            <?=$this->section('content')?>
        </div>
    </div>
</div>

<?php $this->insert('includes/footer') ?>


