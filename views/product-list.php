<?php $this->layout('layouts/main') ?>

<h3>Products</h3>

<table class="table">
    <thead>
    <tr>
        <th>#</th>
        <th>Name</th>
        <th>Description</th>
        <th>Comments</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($products as $product) :  ?>
            <tr>
                <td><?= $product->getId(); ?></td>
                <td> <a href="/products/<?=$product->getId()?>"><?= $product->getName(); ?></a></td>
                <td><?= $product->getDescription(); ?></td>
                <td>
                    <?= $product->getComments()->count(); ?>
                    <? if ($product->getUnreadComments()->count() > 0): ?>
                        (<?=$product->getUnreadComments()->count(); ?>)
                    <? endif; ?>
                </td>
            </tr>
    <?php endforeach ?>
    </tbody>
</table>