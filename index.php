<?php

use Grey\Commentary\ServiceProvider;
use League\Route\RouteCollection;
use Zend\Diactoros\Response;
use Zend\Diactoros\ServerRequestFactory;

require __DIR__ . "/vendor/autoload.php";

define("BASE_DIR", __DIR__);

$container = new League\Container\Container();

$container->addServiceProvider(new ServiceProvider\RouterServiceProvider());
$container->addServiceProvider(new ServiceProvider\EntityManagerServiceProvider());

$response = $container->get(RouteCollection::class)
    ->dispatch(ServerRequestFactory::fromGlobals(), new Response());

(new Response\SapiEmitter())->emit($response);